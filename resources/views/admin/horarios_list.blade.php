@extends('adminlte::page')

@section('title', 'Cadastro de Horários')

@section('content_header')
    <h1>Cadastro de Horarios</h1>
    <h1>{{$informacao}}</h1>
   
    
   


    <a href="{{ route('horarios.create') }}" class="btn btn-primary pull-right"
       role="button">Novo</a>
    </h1>
@stop

@section('content')

@if (session('status'))
   <div class="alert alert-success">
      {{ session('status') }}
   </div> 
@endif

<table class="table table-striped">
  <thead>
    <tr>
      <th>Linha</th>
      <th>Tipo</th>
      <th>Descrição</th>
      <th>Hora</th>     
      <th>Sentido</th>
      <th>Ações</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($linhas as $l)
      <tr>
        <td> {{$l->linha_id}} </td>
        <td> {{$l->tipo_id}} </td>
        <td> {{$l->descricao}} </td>
        <td> {{$l->horario }} </td>
        <td> {{$l->sentido }} </td>
          
       
        <td> 
            <a href="{{route('horarios.edit', $l->id)}}" 
                class="btn btn-warning btn-sm" title="Alterar"
                role="button"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
            <form style="display: inline-block"
                  method="post"
                  action="{{route('horarios.destroy', $l->id)}}"
                  onsubmit="return confirm('Confirma Exclusão?')">
                   {{method_field('delete')}}
                   {{csrf_field()}}
                  <button type="submit" title="Excluir"
                          class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
            </form>
        </td>
       
        @if ($loop->iteration == $loop->count)
             <tr><td colspan=8>Total de Horarios cadastrados: {{$numHorarios}}                              
                              </td></tr>
        @endif        
    @empty
      <tr><td colspan=8> Não há Horários cadastrados ou 
                         para o filtro informado </td></tr>
    @endforelse

  </tbody>
</table>  

{{ $linhas->links() }}
@stop

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection

