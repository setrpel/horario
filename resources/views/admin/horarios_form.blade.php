@extends('adminlte::page')

@section('title', 'Cadastro de Horários')

@section('content_header')
 
    @if ($acao==1)
       <h2>Inclusão de Horários
    @elseif ($acao ==2)
       <h2>Alteração de Horários
    @endif          

    <a href="{{ route('horarios.index') }}" class="btn btn-primary pull-right"
       role="button">Voltar</a>
    </h2>

@endsection

@section('content')

   <div class="container-fluid">

    @if ($acao==1)
      <form method="POST" action="{{ route('horarios.store') }}" enctype="multipart/form-data">
    @elseif ($acao==2)
      <form method="POST" action="{{route('horarios.update', $reg->id)}}" enctype="multipart/form-data">
      {!! method_field('put') !!}
    @endif          
    {{ csrf_field() }}

    <div class="col-sm-6">
        <div class="form-group">
          <label for="linhas_id">Nome da Linha</label>
          <select id="linhas_id" name="linhas_id" class="form-control">
            @foreach($linhas as $l)
              <option value="{{$l->id}}" 
                      {{ ((isset($reg) and $reg->linhas_id == $l->id) or 
                         old('linhas_id') == $l->id) ? "selected" : "" }}>
                      {{$l->nome}}</option>
            @endforeach
          </select>  
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="tipo_id">Tipo da Linha</label>
          <select id="tipo_id" name="tipo_id" class="form-control">
            @foreach($tipos as $t)
              <option value="{{$t->id}}" 
                      {{ ((isset($reg) and $reg->tipo_id == $t->id) or 
                         old('tipo_id') == $t->id) ? "selected" : "" }}>
                      {{$t->nome}}</option>
            @endforeach
          </select>  
        </div>
      </div>
                

    
      
        <div class="col-sm-6">
          <div class="form-group">
              <label for="horario">Horário (digite nesse formato: 2000-01-01 00:00:00)</label>
              <input type="text" id="horario" name="horario" required 
                     value="{{$reg->horario or old('horario')}}"
                     class="form-control">
          </div>
        </div>
    

    <div class="row">
        
      
        <div class="col-sm-4">
          <div class="form-group">
              <label for="sentido">Sentido</label>
              <select id="sentido" name="sentido" class="form-control">
                  @foreach($sent as $s)
                    <option value="{{$s}}"{{isset($reg) && $reg->sintido == $s || old('sentido') == $s ? "selected":""}}>{{$s}}</option>
                  @endforeach
              </select>  
            </div>
        </div>

       
        
      
        
    </div>
    <div class="col-sm-6">
        <div class="form-group">
        
          <select id="users-id" name="users_id" class="form-control" style="display:none">
            @foreach($users as $u)
              <option id="users-id" value="{{$u->id}}" >
                     </option>
            @endforeach
          </select>  
        </div>
      </div>

    <div class="col-sm-4">
    <input type="submit" value="Enviar" class="btn btn-success">
    <input type="reset" value="Limpar" class="btn btn-warning">
    </div>
    </form>
  </div>

@endsection

@section('js')
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="/js/jquery.mask.min.js"></script>

  <script>
    $(document).ready(function() {
      $('#horario').mask('####-##-## ##:##:##', {reverse: true});
    });
  </script>  
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif