@extends('adminlte::page')

@section('title', 'Cadastro de Linhas')

@section('content_header')
 
    @if ($acao==1)
       <h2>Inclusão de Linhas
    @elseif ($acao ==2)
       <h2>Alteração de Linhas
    @endif          

    <a href="{{ route('linhas.index') }}" class="btn btn-primary pull-right"
       role="button">Voltar</a>
    </h2>

@endsection

@section('content')

   <div class="container-fluid">

    @if ($acao==1)
      <form method="POST" action="{{ route('linhas.store') }}" enctype="multipart/form-data">
    @elseif ($acao==2)
      <form method="POST" action="{{route('linhas.update', $reg->id)}}" enctype="multipart/form-data">
      {!! method_field('put') !!}
    @endif          
    {{ csrf_field() }}

    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="nome">Nome do Linha</label>
          <input type="text" id="nome" name="nome" required 
                 value="{{$reg->nome or old('nome')}}"
                 class="form-control">
        </div>
      </div>

      
    </div>              

    
      
       
    </div>

    <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="ano">Descrição</label>
            <input type="text" id="descricao" name="descricao" required 
                   value="{{$reg->descricao or old('descricao')}}"
                   class="form-control">
          </div>
        </div>
      
       

        <div class="col-sm-4">
          <div class="form-group">
            <label for="foto">Foto</label>
            <input type="file" id="foto" name="foto" required
                   class="form-control">
          </div>
        </div>
        
    </div>

    <input type="submit" value="Enviar" class="btn btn-success">
    <input type="reset" value="Limpar" class="btn btn-warning">

    </form>
  </div>

@endsection

@section('js')
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="/js/jquery.mask.min.js"></script>

  
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif