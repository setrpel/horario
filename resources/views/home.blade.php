@extends('site1')

@section('conteudo')


<div class="container-center">
<div div style='text-align:center'>
 
  <font color=#dfe4ed size="5" >
     <H1> NOME DA EMPRESA</h1>
     
     <a style="color: white;padding-right: 10%;" href="/uteis">Segunda a Sexta-Feira</a>

     <a style="color: white;padding-right: 10%;" href="/sabado">Sábados</a> 
     
     <a style="color: white;"href="/domingo">Domingos</a>
     </font>
</div>
</div>


	
<div class="container-center">
<div div style='text-align:center'>
 
  <font color=#dfe4ed >
     <H3 ">{{$tipo}}</H3>
     </font>
</div>
</div>
 

  


@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif
</div>



<div class="container">
	<div class="row">
		
			
  
        <div class="form-group">
          <label for="linhas_id" style = "color:white" >Nome da Linha</label>
          <select id="linhas_id" name="linhas_id" class="form-control">
            @foreach($linhas as $l)
              <option value="{{$l->id}}" 
                      {{ ((isset($reg) and $reg->linhas_id == $l->id) or 
                         old('linhas_id') == $l->id) ? "selected" : "" }}>
                      {{$l->nome}}</option>
            @endforeach
          </select>  
        </div>
     
				
			
			<div class="thumbnail">			
			<div class="modal-body">
				<div class="row">
				
			
				<div class="col-md-6">
				
        <table class="table table-hover" div style='text-align:center'>
        <H3 div style='text-align:center'>CENTRO</H3>
    <thead >
    <tr>

   
 
    
      <th div style='text-align:center'>Horário</th>   
      </tr>
      </thead>
  <tbody >
    @forelse ($centro as $c)
    <tr>
     
     
            <td> {{$c->horario}} </td>
            </tr>
  @empty
  @endforelse
   
  </tbody>
  
	
</table>								
							
							</div>

				<div class="col-md-6">
        <table class="table table-hover" div style='text-align:center'>
        <H3 div style='text-align:center'>BAIRRO</H3>
    <thead>
    <tr>

   
  
      <th  div style='text-align:center'>Horário</th>   
      </tr>
      </thead>
  <tbody>
    @forelse ($bairro as $b)
    <tr>
   
    
            <td> {{($b->horario) }} </td>
            </tr>
  @empty
  @endforelse
  
  </tbody>
  
	
</table>		
			</div>

					</div>
          </div>
							
								<div class="modal-footer">
				
		
				</div>				
							</form>
					</div>
				</div>
			
			
                </div>
	</div>
</div>
	


 	<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-latest.min.js"></script>
	<script src="/js/jquery.mask.min.js"></script>
	<script>
  		
  		});
	</script>
@endsection
