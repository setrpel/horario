<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell):

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Pulse Storm](http://www.pulsestorm.net/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## HORARIOS
**API HORARIOS COM FRAMEWORK LARAVEL**

Criação do banco de dados Mysql com o nome: horários

Migração da estrutura de tabelas:

`  $  php artisan migrate`

Inicialização do aplicativo

` $   php artisan serve `

## Documentação API

Listar Horários lista os horários da rota api/tabelas
   
    **GET** /tabelas

Retorna lista de informações sobre o cadastro de horários

  
  
    {
        "id": 10,
        "prefixo": "100",
        "horario": "02:00:00",
        "dia": "UTEIS",
        "sentido": "Bairro",
        "linha": "FRAGATA"
    },
    {
        "id": 1,
        "prefixo": "200",
        "horario": "00:20:00",
        "dia": "UTEIS",
        "sentido": "Bairro",
        "linha": "CRUZEIRO"
    },


Listar um Id dos horários:

    GET /tabelas/:id
    
    exemplo
    
    **GET** /tabelas/1
    

Retorno do horário

    {
        "id": 1,
        "prefixo": "200",
        "horario": "00:20:00",
        "dia": "UTEIS",
        "sentido": "Bairro",
        "linha": "CRUZEIRO"
    },


**Update** de prefixo na tabela horarios

    PUT /tabelas/10
    
Você precisa enviar um número de prefixo nessa estrutura:

{

    "prefixo": "100",
      
}
      
Retorna:


    "id": 10,
    "sentido": "Bairro",
    "horario": "02:00:00",
    "tipo_id": 1,
    "users_id": 1,
    "linhas_id": 4,
    "prefixo": "100",
    "created_at": "2019-11-30 22:08:46",
    "updated_at": "2019-11-30 23:49:33"





## Documentação dos Métodos
**APP/HTTP
/CONTROLLERS/ADMIN**
 – Controllers que para serem acionados estão dentro da área administrativa.
 
**HorarioController** tem a função de manipular todas as informações referentes a tabela horarios, seus relacionamentos, gráficos e relatórios.

**Index** Lista os dados da tabela:

```
public function index()
    {
        $informacao = 'Todos Horários';
        $dados = Horario::orderBy('horarios.horario')->paginate(30);

        
        $numHorarios = Horario::count('id'); 

        return view('admin.horarios_list', ['linhas' => $dados, 'informacao' => $informacao,

            'numHorarios' => $numHorarios]);
    }
```



**HoráriosUC / HoráriosUB / HoráriosSC / HoráriosSB / HoráriosDB / HoráriosDB** são funções para retornar os horários separados por tipos de tabelas e sentido das linhas.


 ```
public function horariosUC()
    {
        $informacao = 'Dias Úteis / Sentido Centro';
        $dados = Horario::orderBy('horarios.horario')->WHERE('horarios.sentido', '=', 'C')
            ->WHERE('horarios.tipo_id', '=', '1')->paginate(30);

        $numHorarios = Horario::count('id'); // obtém o número de registros

        return view('admin.horarios_list', ['linhas' => $dados, 'informacao' => $informacao,

            'numHorarios' => $numHorarios]);

    }
```


**CREATE** Retorna a View para criar um item da tabela:


```
public function create()
    {

        $linhas = Linha::orderBy('nome')->get();
        $tipos = Tipo::orderBy('nome')->get();
        $users = User::orderBy('name')->get();
        $sentidos = Horario::sentido();
        return view('admin.horarios_form', ['tipos' => $tipos, 'linhas' => $linhas,
            'users' => $users, 'acao' => 1,
            'sent' => $sentidos]);

    }
```



**STORE** Salva o novo item na tabela


```
public function store(Request $request)
    {
        // obtém todos os campos vindos do form
        $dados = $request->all();

        $inc = Horario::create($dados);

        if ($inc) {
            return redirect()->route('horarios.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
```


**EDIT** Retorna a View para edição do dado

```
public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Horario::find($id);
        $users = User::orderBy('name')->get();
        $linhas = Linha::orderBy('nome')->get();
        $tipos = Tipo::orderBy('nome')->get();
        $sentidos = Horario::sentido();

        return view('admin.horarios_form', ['reg' => $reg, 'tipos' => $tipos, 'linhas' => $linhas,
            'users' => $users, 'sent' => $sentidos,
            'acao' => 2]);
    }
```


**UPDATE** Salva a atualização do dado

```
public function update(Request $request, $id)
    {
        // obtém os dados do form
        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Horario::find($id);

        // realiza a alteração
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('horarios.index')
                ->with('status', $request->nome . ' Alterado!');
        }

    }
```


**DESTROY** Remove o dado

```
public function destroy($id)
    {
        $horario = Horario::find($id);
        if ($horario->delete()) {
            return redirect()->route('horarios.index')
                ->with('status', $horario->nome . ' Excluído!');
        }
    }
```


**RELATORIO** função que contem consultas para alimentar um relatório de linhas e horários

 ```
public function relatorio()
    {

        $centroU = Horario::join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
            ->join('linhas', 'horarios.linhas_id', '=', 'linhas.id')
            ->WHERE('tipos.nome', '=', 'ÚTEIS')->WHERE('horarios.sentido', '=', 'C')
            ->select('horarios.prefixo as prefixo',
                'horarios.horario', 'tipos.nome', 'horarios.sentido', 'linhas.nome as nome')
            ->orderBy('horarios.prefixo')->get();

        $bairroU = Horario::join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
            ->join('linhas', 'horarios.linhas_id', '=', 'linhas.id')
            ->WHERE('tipos.nome', '=', 'ÚTEIS')->WHERE('horarios.sentido', '=', 'B')
            ->select('horarios.prefixo as prefixo',
                'horarios.horario', 'tipos.nome', 'horarios.sentido', 'linhas.nome as nome')
            ->orderBy('horarios.prefixo')->get();

        $centroS = Horario::join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
            ->join('linhas', 'horarios.linhas_id', '=', 'linhas.id')
            ->WHERE('tipos.nome', '=', 'SÁBADOS')->WHERE('horarios.sentido', '=', 'C')
            ->select('horarios.prefixo as prefixo',
                'horarios.horario', 'tipos.nome', 'horarios.sentido', 'linhas.nome as nome')
            ->orderBy('horarios.prefixo', 'horarios.horario')->get();

        $bairroS = Horario::join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
            ->join('linhas', 'horarios.linhas_id', '=', 'linhas.id')
            ->WHERE('tipos.nome', '=', 'SÁBADOS')->WHERE('horarios.sentido', '=', 'B')
            ->select('horarios.prefixo as prefixo',
                'horarios.horario', 'tipos.nome', 'horarios.sentido', 'linhas.nome as nome')
            ->orderBy('horarios.prefixo', 'horarios.horario')->get();

        $centroD = Horario::join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
            ->join('linhas', 'horarios.linhas_id', '=', 'linhas.id')
            ->WHERE('tipos.nome', '=', 'DOMINGOS')->WHERE('horarios.sentido', '=', 'C')
            ->select('horarios.prefixo as prefixo',
                'horarios.horario', 'tipos.nome', 'horarios.sentido', 'linhas.nome as nome')
            ->orderBy('horarios.prefixo', 'horarios.horario')->get();

        $bairroD = Horario::join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
            ->join('linhas', 'horarios.linhas_id', '=', 'linhas.id')
            ->WHERE('tipos.nome', '=', 'DOMINGOS')->WHERE('horarios.sentido', '=', 'B')
            ->select('horarios.prefixo as prefixo',
                'horarios.horario', 'tipos.nome', 'horarios.sentido', 'linhas.nome as nome')
            ->orderBy('horarios.prefixo', 'horarios.horario')->get();

        return \PDF::loadView('admin.horarios_rel', ['centroU' => $centroU, 'bairroU' => $bairroU,
            'centroS' => $centroS, 'bairroS' => $bairroS,
            'centroD' => $centroD, 'bairroD' => $bairroD])->stream();

    }
```



**GRAFICO** função que contem consultas para alimentar um gráfico de linhas e horários

```
public function grafico()
    {
        $sql = "select linhas.nome, count(horarios.linhas_id) as total from horarios,
        linhas where linhas.id = horarios.linhas_id group by linhas.nome order by 2 desc, 1";
        $totais = DB::select($sql);

        return view('admin.horarios_graf', ['totais' => $totais]);
    }
```


 **LinhaController** tem a função de manipular todas as informações referentes a tabela linhas.
 
**INDEX** Lista os dados da tabela

```
public function index()
    {
        $dados = Linha::paginate(3);
        // obtém a soma do campo preço
        $numLinhas = Linha::count('id'); // obtém o número de registros
        return view('admin.linhas_list', ['linhas' => $dados,
            'numLinhas' => $numLinhas]);
    }
```


**CREATE** – Retorna a View para criar um item da tabela

```
$inc = Linha::create($dados);
        if ($inc) {
            return redirect()->route('linhas.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
```


**EDIT** Retorna a View para edição do dado	

```
public function edit($id)
    {
         // posiciona no registro a ser alterado e obtém seus dados
         $reg = Linha::find($id);
         $linhas = Linha::orderBy('nome')->get();
          return view('admin.linhas_form', ['reg' => $reg,  
                                           'acao' => 2]);
    }
```


**UPDATE** Salva a atualização do dado

```
 public function update(Request $request, $id)
    {
        // obtém os dados do form
        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Linha::find($id);

        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {

            if (Storage::exists($reg->foto)) {
                Storage::delete($reg->foto);
            }

            $path = $request->file('foto')->store('fotos');
            
            $dados['foto'] = $path;
        }

        // realiza a alteração
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('linhas.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
    }
```

**DESTROY** Remove o dado

```
public function destroy($id)
    {
        $linha = Linha::find($id);
        if ($linha->delete()) {
            return redirect()->route('linhas.index')
                            ->with('status', $linha->nome . ' Excluído!');
        }
```

**TipoController** tem a função de manipular todas as informações referentes a tabela tipos.

**INDEX** Lista os dados da tabela

  ```
public function index()
    {
        $dados = Tipo::paginate(30);
        return view('admin.tipos_list', ['tipos' => $dados]);
    }
```


**CREATE** – Retorna a View para criar um item da tabela

```
public function create()
    {
        return view('admin.tipos_form', ['acao' => 1]);
    }
```


**STORE** Salva o novo item na tabela

```
public function store(Request $request)
    {
        // obtém todos os campos vindos do form
        $dados = $request->all();
        $inc = Tipo::create($dados);
        if ($inc) {
            return redirect()->route('tipos.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
```


**EDIT** Retorna a View para edição do dado	

```
public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Tipo::find($id);
        $linhas = Tipo::orderBy('nome')->get();
        return view('admin.tipos_form', ['reg' => $reg, 'acao' => 2]);
    }
```


**UPDATE** Salva a atualização do dado


```
public function update(Request $request, $id)
    {
        // obtém os dados do form
        $dados = $request->all();
        // posiciona no registo a ser alterado
        $reg = Tipo::find($id);
        // se o usuário informou a foto e a imagem foi corretamente enviada
        // realiza a alteração
        $alt = $reg->update($dados);
        if ($alt) {
            return redirect()->route('tipos.index')
                ->with('status', $request->nome . ' Alterado!');
        }
    }
```


**DESTROY** Remove o dado

  ```
public function destroy($id)
    {
        $tipos = Tipo::find($id);
        if ($tipos->delete()) {
            return redirect()->route('tipos.index')
                ->with('status', $tipos->nome . ' Excluído!');
        }
    }
```


## CONTOLLERS/AUTH

Controllers que controlam as permissões de acesso a área autenticada dos sistema.

**ForgotPasswordController** tem a função enviar chaves de acesso por email.

**__construct** Método padrão do Laravel de construção para realizar a conferência e autenticação de chaves para uma nova senha. Utiliza-se do middleware para controlar as requisições.

 ```
public function __construct()
    {
        $this->middleware('guest');
    }
```


**LoginController** tem a função entrar na área restrita ou desvincular o usuário da área restrita.

**__construct** Método padrão do Laravel de construção para realizar a autenticação no sistema. Utiliza-se do middleware para controlar as requisições.


```
 public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
```




**RegisterController** tem a função registrar usuários.

**__construct** Método padrão do Laravel de construção instanciar as chamadas na midleware.

 ```
public function __construct()
    {
        $this->middleware('guest');
    }
```


**VALIDATOR** tem a função de validar se os dados de inserção no banco correspondem com as configuradas no banco de dados.

```
protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
```


**CREATE** – Retorna a View para criar um item da tabela

```
protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
```


**ResetsPasswords** tem a função alterar a senha de acesso.

**__construct** Método padrão do Laravel de construção instanciar as chamadas na midleware.

 **public function __construct()
    {
        $this->middleware('guest');
    }**


## ACESSO PÚBLICO

## HTTP/CONTROLLERS

**HomeController** tem a função de retornar a página inicial do sistema os horários e linhas registradas no banco de dados.

**Index** Lista os dados da tabela: 

Inicialmente queremos um retorno nulo a pesquisa, mas necessitamos da informação da variáveis portanto coloquei uma comparação entre tabelas que irá retornar nenhum registro encontrado.
   
   ```
 public function index()
    {
        $linhas = Linha::orderBy('nome')->get();
        $tipo = "";
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome,
      tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
      INNER join linhas on horarios.linhas_id = linhas_id
      where tipos.nome = 'ÚTEIS1' and horarios.sentido = 'C' order by horarios.horario";
        $centro = DB::select($sql);
        $bairro = DB::select($sql);
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');
    }
```


**UTEIS / DOMINGO / SABADO** são funções para retornar os horários separados por tipos de tabelas. 

  ```
public function uteis()
    {
      $linhas = Linha::orderBy('nome')->get();
        $tipo = "Horários de Segunda a Sexta-feira";
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome AS linha,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
         where tipos.nome = 'ÚTEIS' and horarios.sentido = 'C' order by horarios.horario";
        $centro = DB::select($sql);
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome AS linha,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
      where tipos.nome = 'ÚTEIS' and horarios.sentido = 'B' order by horarios.horario";
        $bairro = DB::select($sql);
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');
    }
    public function domingo()
    {
      $linhas = Linha::orderBy('nome')->get();
        $tipo = "Horários de Domingos e Feriados";
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome AS linha,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
         where tipos.nome = 'DOMINGOS' and horarios.sentido = 'C' order by horarios.horario";
        $centro = DB::select($sql);
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome AS linha,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
      where tipos.nome = 'DOMINGOS' and horarios.sentido = 'B' order by horarios.horario";
        $bairro = DB::select($sql);
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');
    }
    public function sabado()
    {
      $linhas = Linha::orderBy('nome')->get();
        $tipo = "Horários de Sábados";
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome AS linha,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
         where tipos.nome = 'SÁBADOS' and horarios.sentido = 'C' order by horarios.horario";
        $centro = DB::select($sql);
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome AS linha,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
      where tipos.nome = 'SÁBADOS' and horarios.sentido = 'B' order by horarios.horario";
        $bairro = DB::select($sql);
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');
    }
```

**JSController** serve para controlar as requisições no banco em formato JSON. É a controller que irá retornar os dados a serem consumidos pelo sistema de ingresso de veículos.

**INDEX **Lista os dados da tabela no formato da consulta desejada

```
public function index()
    {
        $horarios = Horario::join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
        ->join('linhas', 'horarios.linhas_id', '=', 'linhas.id')
        ->select ('horarios.id','horarios.prefixo as prefixo',
    'horarios.horario','tipos.nome as dia', 'horarios.sentido', 'linhas.nome as linha')
        ->orderBy('tipos.nome', 'horarios.horario')->get();
        return response()
        ->json($horarios, 200, [], JSON_PRETTY_PRINT);
    }
```


**SHOW** Mostra um item específico

```
public function show($id)
    {
        $reg = Horario::find($id);
        if($reg){
            return response()
            ->json($reg, 200,[], JSON_PRETTY_PRINT);
        }else {
            return response()
            ->json(['error'=> 'not_found'],404);
        }
    }
```

**STORE** Salva o novo item na tabela


```
public function store(Request $request)
    {
        $dados = $request->all();
        $inc = Horario::create($dados);
        if($inc){
            return response()->json([$inc], 201);
        }else{
            return response()->json(['error'=>'error_insert'],500);
        }
    }
```


**UPDATE** Salva a atualização do dado


```
public function update(Request $request, $id)
    {
        $reg = Horario::find($id);
        if($reg){
 $dados = $request->all();
 $alt = $reg->update($dados);
 if($alt){
            return response()
            ->json($reg, 200,[], JSON_PRETTY_PRINT);
        }else {
            return response()
            ->json(['error'=> 'not_update'],500);
     }
 }else {
         return response()
         ->json(['error'=> 'not_found'],404);
  }
```


**DESTROY** Remove dado

```
public function destroy($id)
    {
        $reg = Horario::find($id);
        if($reg){
 if($reg->delete()){
            return response()
            ->json(['msg'=>'Ok! Excluído'], 200);
        }else {
            return response()
            ->json(['error'=> 'not_destroy'],500);
     }
 }else {
         return response()
         ->json(['error'=> 'not_found'],404);
  }
```


## HTTP/MIDDLEWARE/

Camada do sistema que distribui as requisições, autenticações entre outros. No sistema horários utilizamos o Cors para podermos liberar as controllers de máquinas distintas como as classes de autenticação de usuários, chaves de acesso se fossemos usar etc.

**Cors** controla acesso entre maquinas diferentes pela função HANDLE, nesse caso autorizei qualquer tipo de consulta, inserção, alteração ou exclusão, mas poderia negar alguns métodos como exemplo DELETE, se removesse não poderíamos deletar.
  
 ```
 public function handle($request, Closure $next)
    {
        return $next($request)
        ->header('Acess-Control-Allow-Origin','*')
        ->header('Acess-Control-Allow-Methods','GET,POST,PUT,DELETE,OPTIONS');
    }
```


**EncryptCookies** o framework Laravel traz para controlar cookies caso necessário.

**RedirectIfAuthenticated** o framework Laravel traz para controlar os redirecionamentos de autenticação, usamos a função HANDLE para redirecionar a home caso não esteja autenticado.
 
 ```
public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }
        return $next($request);
    }
```


**TrimStrings** Como o nome indica, esse middleware remove espaços extras do início e do final dos dados da solicitação automaticamente.

**TrustProxies** Em um nível alto, o Trusted Proxy informa ao Laravel sobre proxies confiáveis e como mapear X-Forwarded-*cabeçalhos a partir da solicitação.

```
protected $headers = [
        Request::HEADER_FORWARDED => 'FORWARDED',
        Request::HEADER_X_FORWARDED_FOR => 'X_FORWARDED_FOR',
        Request::HEADER_X_FORWARDED_HOST => 'X_FORWARDED_HOST',
        Request::HEADER_X_FORWARDED_PORT => 'X_FORWARDED_PORT',
        Request::HEADER_X_FORWARDED_PROTO => 'X_FORWARDED_PROTO',
    ];
```


**VerifyCsrfToken** verifica a necessidade de Token, no nosso projeto não foi utilizado.

```
protected $except = [
        'api/*'
    ];
```


## HTTP/
**Kernel** pode ser interpretado como uma grande caixa preta a qual você alimenta com requisições (requests) e ela te devolve respostas (responses). Sua implementação reside em lugar específico dessa grande caixa preta.

**Middlewares** usadas

 ```
protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \Barryvdh\Cors\HandleCors::class,
    ];
```


grupos de middleware pra controlar os acessos

```
protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
        'api' => [
            'throttle:60,1',
            'bindings',
            \Barryvdh\Cors\HandleCors::class,
        ],
    ];
```


Rotas das middlewares

```
protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'cors' => \App\Http\Middleware\Cors::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];
```


## HTTP/PROVIDERS 
 
conjunto de provedores de serviço para comunicar as rotas e serviços da programação com os containers do framework.

Exemplo RouteServiceProvider determina as rotas para realizar essa comunicação interna no caso abaixo o mapeamento dos acessos web e api.

```
protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }
  protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
```

## APP/

**Horario** relaciona as views e controllers com a estrutura do banco de dados da tabela horarios, como estruturas de chaves externas, proteção e manipulação de atributos.
 
 ```
protected $fillable = ['tipo_id', 'horario','prefixo', 'sentido', 'linhas_id', 'users_id'];
    public function tipos() {
        return $this->belongsTo('App\Tipo');
    }
    public function linhas() {
        return $this->belongsTo('App\Linha');
    }
    public function users() {
        return $this->belongsTo('App\User');
    }
    public function getTipoAttribute($value) {
        return strtoupper($value);
    }
    public static function sentido() {
        return ['Bairro', 'Centro'];
    }
    public function getSentidoAttribute($value) {
        if ($value=="B") {
            return "Bairro";
        } else if ($value == "C") {
            return "Centro";
        } 
    }
    public function setSentidoAttribute($value) {
        if ($value == "Bairro") {
            $this->attributes['sentido'] = "B";
        } else if ($value == "Centro") {
            $this->attributes['sentido'] = "C";
        } 
```


**Linha** relaciona as views e controllers com a estrutura do banco de dados da tabela linhas, como estruturas de chaves externas, proteção e manipulação de atributos.

```
protected $fillable = ['nome', 'descricao', 'foto', 'users_id' ];
    public function users() {
        return $this->belongsTo('App\User');
    }
```


**Tipo** relaciona as views e controllers com a estrutura do banco de dados da tabela tipos, como estruturas de chaves externas proteção e manipulação de atributos.

```
class Tipo extends Model
{
    //
    protected $fillable = ['nome', 'descricao'];
}
```


User relaciona as views e controllers com a estrutura do banco de dados da tabela users, como estruturas de chaves externas proteção e manipulação de atributos.

```
protected $fillable = [
        'name', 'email', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
```


## CONFIG/

Adminlte configura a estrutura do acesso interno utilizando a estrutura do framework adminlte.

Exemplo menu esquerdo de rotas:

 ```
'menu' => [
        'ÁREA RESTRITA   ',
        [
            'text'    => 'Cadastros',
            'icon'    => 'book',
            'submenu' => [
                [
                    'text' => 'Tipos',
                    'icon'    => 'list',
                    'url'  => 'admin/tipos',
                ],
                [
                    'text' => 'Horários Úteis / Centro',
                    'icon'    => 'list',
                    'url'  => 'admin/horariosUC',
                ],
                [
                    'text' => 'Horários Úteis / Bairro',
                    'icon'    => 'list',
                    'url'  => 'admin/horariosUB',
                ],
                [
                    'text' => 'Horários Sábados / Centro',
                    'icon'    => 'list',
                    'url'  => 'admin/horariosSC',
                ],
                [
                    'text' => 'Horários Sábados / Bairro',
                    'icon'    => 'list',
                    'url'  => 'admin/horariosSB',
                ],
                [
                    'text' => 'Horários Domingos / Centro',
                    'icon'    => 'list',
                    'url'  => 'admin/horariosDC',
                ],
                [
                    'text' => 'Horários Domingos / Bairro',
                    'icon'    => 'list',
                    'url'  => 'admin/horariosDB',
                ],
                [
                    'text' => 'Linhas',
                    'icon'    => 'list',
                    'url'  => 'admin/linhas',
                ],
            ],
        ],
        [
            'text'    => 'Gráficos',
            'icon'    => 'signal',
            'submenu' => [
                [
                    'text' => 'Horários por Linhas',
                    'icon'    => 'pie-chart',
                    'url'  => 'admin/grafhorarios',
                ],
            ],
        ],  
        [
            'text'        => 'Relatórios',
            'icon'        => 'print',
            'label_color' => 'danger',
            'submenu' => [
                [
                    'text' => 'Horários',
                    'icon' => 'file-pdf-o',
                                        'url'  => 'admin/horariosPDF',
                ],  
            ],       
        ],   
            ],
```


**App** configura estrutura básica do sistema, como nome, endereço, localidade global, idioma, chave de controle, provedores, bibliotecas importadas, etc.

**Auth** configura estrutura básica de acesso ao sistema, como autorização de senhas e tokens.

**Broadcasting** A finalidade é trabalhar com eventos em tempo Real ex: tem 2 páginas abertas uma que lista os usuários e outra que adiciona, Ao adicionar um novo, a página que lista automaticamente recebe o novo item.

**Cache** tem a finalidade de determinar o que deverá ficar em cache no browser.

**Cors** tem a finalidade de autorizar acessos de maquinas distintas.

```
return [
    'supportsCredentials' => false,
    'allowedOrigins' => ['*'],
    'allowedOriginsPatterns' => [],
    'allowedHeaders' => ['Content-Type', 'X-Requested-With'],
    'allowedMethods' =>['GET', 'POST', 'PUT',  'DELETE'],
    'exposedHeaders' => [],
    'maxAge' => 0,
];
```


**Databases** configurações padrão de conexão com bancos de dados, mysql, sqlite, postgre, sqlsrv, além de migrações. 

**Filesystems** Neste arquivo você pode configurar todos os seus "discos". Cada disco representa um driver de armazenamento particular e o local de armazenamento. No arquivo de configuração você encontra exemplos para cada driver suportado. Então, simplesmente modifique a configuração para refletir as suas preferências e credenciais do seu armazenamento.

```
'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],
    ],
```


**Mail** controla a estrutura de envios de e-mails.

**Queue** controla filas para despachar em seu programa.

**Services** Este arquivo é para armazenar as credenciais para serviços de terceiros, como Stripe, Mailgun, SparkPost e outros.

**Session** define padrões de sessão, como por exemplo o tempo que uma sessão ficará ativa sem expirar.	 

  ```
'lifetime' => env('SESSION_LIFETIME', 120),
    'expire_on_close' => false,
```


**View** determina local onde serão buscadas as views padrão do framework

## DATABASES/FACTORIES/

**UserFactory** Este diretório deve conter cada uma das definições de fábrica de modelo para sua aplicação.
 
## DATABASES/MIGRATIONS/

serve para configurar através do Laravel a estrutura de tabelas do banco de dados

Exemplo de migração no nosso sistema seira a criação da tabela horarios.

```
public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sentido',1);
            $table->time('horario');
            $table->integer('tipo_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->integer('linhas_id')->unsigned();
            $table->integer('prefixo')->unsigned();
            $table->timestamps();
            $table->foreign('tipo_id')->references('id')->on('tipos');
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('linhas_id')->references('id')->on('linhas');
        });
    }
```


## DATABASES/SEEDS 

popula as tabelas do bando de dados. Exemplo a população da tabela tipos.

```
public function run()
    {
        DB::table('tipos')->insert([
            'nome' => 'UTEIS',
            'descricao' => 'Dias Úteis',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('tipos')->insert([
            'nome' => 'SABADOS',
            'descricao' => 'Tabelas de Sábados',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('tipos')->insert([
            'nome' => 'DOMINGOS',
            'descricao' => 'Tabelas de Domingos e Feriados',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
    }
```


## PUBLIC
local onde fica os acessos públicos pelo sistema, como exemplo o armazenamento de imagens na pasta storage, o index ou api de cep por exemplo.

## RESOURCES
nessa parte do sistema configuramos, algumas informações como arquivos de linguagem para traduções, componente js, mas o mais importante para a programação são as views.

## RESOURCES/VIEWS/
aqui encontraremos toda a codificação de front-end para visualização do usuário.

**ADMIN/** todas as blades do acesso interno para realizar a interface com o usuário.
 
**AUTH/** todas as blades de autenticação para realizar a interface com o usuário (nesse caso ele estende as configurações do adminlte)
 
**RESOURCES/VIEWS/** home e site blades do acesso inicial do sistema.
 
Apresentaremos a blade linhas_form como exemplo.

```
@extends('adminlte::page')
@section('title', 'Cadastro de Linhas')
@section('content_header')
    @if ($acao==1)
       <h2>Inclusão de Linhas
    @elseif ($acao ==2)
       <h2>Alteração de Linhas
    @endif          
    <a href="{{ route('linhas.index') }}" class="btn btn-primary pull-right"
       role="button">Voltar</a>
    </h2>
@endsection
@section('content')
   <div class="container-fluid">
    @if ($acao==1)
      <form method="POST" action="{{ route('linhas.store') }}" enctype="multipart/form-data">
    @elseif ($acao==2)
      <form method="POST" action="{{route('linhas.update', $reg->id)}}" enctype="multipart/form-data">
      {!! method_field('put') !!}
    @endif          
    {{ csrf_field() }}
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="nome">Nome do Linha</label>
          <input type="text" id="nome" name="nome" required 
                 value="{{$reg->nome or old('nome')}}"
                 class="form-control">
        </div>
      </div>
    </div>              
    </div>
    <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="ano">Descrição</label>
            <input type="text" id="descricao" name="descricao" required 
                   value="{{$reg->descricao or old('descricao')}}"
                   class="form-control">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="foto">Foto</label>
            <input type="file" id="foto" name="foto" required
                   class="form-control">
          </div>
        </div>
    </div>
    <input type="submit" value="Enviar" class="btn btn-success">
    <input type="reset" value="Limpar" class="btn btn-warning">
    </form>
  </div>
@endsection
@section('js')
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="/js/jquery.mask.min.js"></script>
@endsection
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
```


## ROUTES/ 

aqui endereçamos todas as rotas da camada mais alta.

**Api** rota para acesso as informações do API.

```
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('tabelas', 'JSController', 
//excessao do create e edit na controller.
['except'=> 'create','edit']);
```

**Web** rota para acessos web nele encontramos alguns controles de middleware para páginas que necessitam de autenticação.

```
Rotas livres:
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/uteis', 'HomeController@uteis')->name('uteis');
Route::get('/sabado', 'HomeController@sabado')->name('sabado');
Route::get('/domingo', 'HomeController@domingo')->name('domingo');
Route::post('homeBusca', 'homeController@homeBusca')->name('homeBusca');
Route::get("/", "homeController@index")->name('home.index');
```


Rotas que necessitam passar pelo middleware e estarem autenticadas (logado).

```
Route::get('/admin', function () {
    return view('admin.index');
})->middleware('auth');
Route::prefix('admin')->group(function () {
    Route::resource('pratos', 'Admin\PratoController')->middleware('auth');
    Route::resource('pedidos', 'Admin\PedidoController')->middleware('auth');
    Route::resource('tipos', 'Admin\TipoController')->middleware('auth');
    Route::get('/grafhorarios', 'Admin\HorarioController@grafico')->middleware('auth');
    Route::resource('horarios', 'Admin\HorarioController')->middleware('auth');
    Route::resource('linhas', 'Admin\LinhaController')->middleware('auth');
    Route::get('/horariosPDF', 'Admin\HorarioController@relatorio');
    Route::get('horariosUC', 'Admin\HorarioController@horariosUC')->middleware('auth');
    Route::get('horariosUB', 'Admin\HorarioController@horariosUB')->middleware('auth');
    Route::get('horariosSC', 'Admin\HorarioController@horariosSC')->middleware('auth');
    Route::get('horariosSB', 'Admin\HorarioController@horariosSB')->middleware('auth');
    Route::get('horariosDC', 'Admin\HorarioController@horariosDC')->middleware('auth');
    Route::get('horariosDB', 'Admin\HorarioController@horariosDB')->middleware('auth');
});
```


## STORAGE/ 

local onde o framework armazena as informações de cache, imagens, logs.
 
## VENDOR/
o diretório está relacionado a aplicações desenvolvido por terceiros que compõem os requisitos da sua aplicação nele importamos bibliotecas para usarmos métodos de gerador de pdf por exemplo.
 

## ./ENV 

onde colocamos as variáveis de ambiente, como exemplo conexão com o bando de dados, endereçamento de email.
 

