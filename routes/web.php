<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//site

Route::post('homeBusca','homeController@homeBusca')->name('homeBusca');
Route::get("/", "homeController@index")->name('home.index');
Route::get('home_detalhes/{id}', 'homeController@detalhes');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/uteis', 'HomeController@uteis')->name('uteis');
Route::get('/sabado', 'HomeController@sabado')->name('sabado');
Route::get('/domingo', 'HomeController@domingo')->name('domingo');

Route::get('/admin', function() {
    return view('admin.index');
})->middleware('auth');

Route::prefix('admin')->group(function () {
    
    Route::resource('horarios', 'Admin\HorarioController')->middleware('auth');
        Route::resource('tipos', 'Admin\TipoController')->middleware('auth');
        Route::resource('linhas', 'Admin\LinhaController')->middleware('auth');
Route::get('/horariosPDF', 'Admin\HorarioController@relatorio');
Route::get('horariosUC', 'Admin\HorarioController@horariosUC')->middleware('auth');
Route::get('horariosUB', 'Admin\HorarioController@horariosUB')->middleware('auth');    
Route::get('horariosSC', 'Admin\HorarioController@horariosSC')->middleware('auth');    
Route::get('horariosSB', 'Admin\HorarioController@horariosSB')->middleware('auth');    
Route::get('horariosDC', 'Admin\HorarioController@horariosDC')->middleware('auth');    
Route::get('horariosDB', 'Admin\HorarioController@horariosDB')->middleware('auth');    
    
    
});

Auth::routes();





