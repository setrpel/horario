<?php

use Illuminate\Database\Seeder;

class HorariosSBTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // SÁBADOS BAIRRO 
DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 05:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 05:30',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 06:00',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA COHAB',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 06:20',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 06:45',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA COHAB CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 07:05',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 07:25',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'AB ÁGD ST.ROSA COHAB',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 07:40',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 07:55',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 08:10',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 08:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 08:50',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 09:10',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 09:30',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'AB ÁGD.ST.ROSA COHAB CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 09:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 10:20',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 10:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'AB ÁGD.ST.ROSA COHAB CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 11:20',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 11:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'AB COHAB ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 12:10',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 12:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA COHAB',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 12:50',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 13:10',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA COHAB CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 13:35',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 14:00',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 14:25',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 14:55',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ST.ROSA COHAB CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 15:20',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 15:45',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA COHAB',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 16:05',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 16:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA COHAB',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 17:00',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 17:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'COHAB ST.ROSA',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 18:10',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 18:40',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ST.ROSA COHAB',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 19:20',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 20:00',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA COHAB',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 20:40',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 21:20',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 21:55',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 22:35',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
        'sentido'=> 'B',
        'horario'=> '2000-01-01 23:25',
        'tipo_id'=> 2,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s'),
    ]
);  
    }
}
