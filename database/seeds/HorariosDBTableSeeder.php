<?php

use Illuminate\Database\Seeder;

class HorariosDBTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          //DOMINGOS BAIRRO
DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 05:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 06:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => ' ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 06:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 07:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 08:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 08:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 09:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 10:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 10:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 11:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 12:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 12:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 13:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 14:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 14:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 15:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'AB ÁGD.ST.ROSA COHAB CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 16:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 16:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'AB ÁGD.ST.ROSA COHAB CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 17:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 18:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 18:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 19:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 20:00',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 20:50',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 21:40',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 22:30',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA CB I E II',
    'sentido'=> 'B',
    'horario'=> '2000-01-01 23:20',
    'tipo_id'=> 3,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
    }
}
