<?php

use Illuminate\Database\Seeder;

class HorariosSCTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SÁBADOS CENTRO
DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 06:20',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 06:20',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 06:40',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 07:00',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 07:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 08:00',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 08:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 08:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 09:40',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 10:05',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 10:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 10:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 11:10',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 11:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 11:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 12:05',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 12:25',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 12:45',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 13:10',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 13:35',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 14:05',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 14:35',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 15:05',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 15:35',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 16:05',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 16:30',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 16:55',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 17:15',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 17:35',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 17:55',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 18:15',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 18:35',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA AB',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 18:55',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 19:20',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 19:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 20:20',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 20:50',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD AB',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 21:20',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 22:00',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 22:35',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 23:20',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-02 00:00',
    'tipo_id'=> 2,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
    ]
);  
    }
}
