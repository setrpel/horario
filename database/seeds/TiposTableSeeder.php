<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos')->insert([
            'nome' => 'Úteis',
            'descricao' => 'tabela para dias úteis',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);

        DB::table('tipos')->insert([
            'nome' => 'Sábados',
            'descricao' => 'tabela para os sábados',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('tipos')->insert([
            'nome' => 'Domingos',
            'descricao' => 'Tabela para os domingos e feriados',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        
       
        
    }
}
