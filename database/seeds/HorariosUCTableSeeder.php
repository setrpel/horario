<?php

use Illuminate\Database\Seeder;

class HorariosUCTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          //HORÁRIOS DE DIAS DA SEMANA ÚTEIS CENTRO.
    
    DB::table('horarios')->insert([
        'nome' => 'ÁGUEDA',
        'descricao' => 'BARRA',
        'sentido'=> 'C',
        'horario'=> '2000-01-01 05:30',
        'tipo_id'=> 1,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]
);   DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'BARRA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 06:20',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 06:40',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 07:00',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 07:30',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 08:00',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 08:30',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 08:50',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 09:15',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 09:40',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 10:05',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 10:30',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 10:50',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 11:10',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 11:30',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 11:50',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 12:05',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 12:25',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ST.ROSA ÁG',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 12:45',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 13:10',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 13:35',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 14:05',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 14:35',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 15:05',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 15:30',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ÁGUEDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 15:50',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 16:00',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 16:15',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 16:30',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 16:45',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 17:00',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 17:10',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 17:20',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ESQUERDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 17:30',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 17:40',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ESQUERDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 17:50',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 18:00',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'COHAB ST.ROSA ESQUERDA',
'sentido'=> 'C',
'horario'=> '2000-01-01 18:15',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB EXP',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 18:30',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 18:35',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 18:55',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 19:20',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'ÁGUEDA ST.ROSA COHAB',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 19:50',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 20:20',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ÁGUEDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 20:50',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 21:20',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'COHAB ST.ROSA ESQUERDA',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 22:00',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-01 22:35',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  DB::table('horarios')->insert([
    'nome' => 'ÁGUEDA',
    'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
    'sentido'=> 'C',
    'horario'=> '2000-01-01 23:20',
    'tipo_id'=> 1,
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]
);   DB::table('horarios')->insert([
'nome' => 'ÁGUEDA',
'descricao' => 'CB I E II COHAB ST.ROSA ÁGD',
'sentido'=> 'C',
'horario'=> '2000-01-02 00:00',
'tipo_id'=> 1,
'created_at' => date('Y-m-d h:i:s'),
'updated_at' => date('Y-m-d h:i:s')
]
);  
    }
}
