<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sentido',1);
            $table->datetime('horario');
            $table->integer('tipo_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->integer('linhas_id')->unsigned();
            $table->timestamps();
            $table->foreign('tipo_id')->references('id')->on('tipos');
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('linhas_id')->references('id')->on('linhas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
