<?php

namespace App\Http\Controllers;

use App\Horario;
use Illuminate\Http\Request;

class HorarioController extends Controller
{

    public function index()
    {
        $horarios = Horario::orderBy('horario')->get();
        return response()->json($horarios, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        $dados = $request->all();

        $inc = Horario::create($dados);

        if ($inc) {
            return response()->json([$inc], 201);
        } else {
            return response()->json(['erro' => 'error_insert'], 500);
        }
    }

    public function show($id)
    {
        $reg = Horario::find($id);
        if ($reg) {
            return response()->json($reg, 200, [], JSON_PETTRY_PRINT);
        } else {
            return response()
                ->json(['erro' => 'not found'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $reg = Horario::find($id);
        if ($reg) {
            $dados = $request->all();

            $alt = $reg->update($dados);

            if ($alt) {
                return response()
                    ->json($reg, 200, [], JSON_PRETTY_PRINT);
            } else {
                return response()
                    ->json(['erro' => 'not update'], 500);
            }
        } else {
            return response()
                ->json(['erro' => 'not found'], 404);
        }
    }

    public function destroy($id)
    {
        $reg = Horario::find($id);

        if ($reg) {
            if ($reg->delete()) {
                return response()
                    ->json(['msg' => 'Ok! Excluído'], 200);
            } else {
                return response()
                    ->json(['erro' => 'not delete'], 500);
            }
        } else {
            return response()
                ->json(['erro' => 'not found'], 404);
        }
    }
}
