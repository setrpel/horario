<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Linha;
class HomeController extends Controller
{
    public function index()
    {

      
       $linhas = Linha::orderBy('nome')->get();
        $tipo = "";
       
       //query apenas para dar um resultado nulo.
       $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, 
      tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
      INNER join linhas on horarios.linhas_id = linhas.id
      where tipos.nome = 'ÚTEIS1' and horarios.sentido = 'C'  order by horarios.horario";
        $centro = DB::select($sql);
        $bairro = DB::select($sql);
       
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');

    }

    public function uteis()
    {

      $linhas = Linha::orderBy('nome')->get();
      
        $tipo = "Horários de Segunda a Sexta-feira";

        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
         where tipos.nome = 'ÚTEIS' and horarios.sentido = 'C' order by horarios.horario";
        $centro = DB::select($sql);

        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
      where tipos.nome = 'ÚTEIS' and horarios.sentido = 'B' order by horarios.horario";
        $bairro = DB::select($sql);

        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');

    }
    public function domingo()
    {
      $linhas = Linha::orderBy('nome')->get();
        $tipo = "Horários de Domingos e Feriados";
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
         where tipos.nome = 'DOMINGOS' and horarios.sentido = 'C' order by horarios.horario";
        $centro = DB::select($sql);

        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
      where tipos.nome = 'DOMINGOS' and horarios.sentido = 'B' order by horarios.horario";
        $bairro = DB::select($sql);
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');

    }
    public function sabado()
    {
      $linhas = Linha::orderBy('nome')->get();
        $tipo = "Horários de Sábados";
        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
         where tipos.nome = 'SÁBADOS' and horarios.sentido = 'C' order by horarios.horario";
        $centro = DB::select($sql);

        $sql = "SELECT DATE_FORMAT(horario, '%H:%i') AS horario, linhas.nome,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipo_id = tipos.id
        INNER join linhas on horarios.linhas_id = linhas.id
      where tipos.nome = 'SÁBADOS' and horarios.sentido = 'B' order by horarios.horario";
        $bairro = DB::select($sql);
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'tipo' => $tipo])->with('msg', ' Excluído!');

    }

}
