<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Horario;
use App\Tipo;
use App\Linha;
use App\User;
class HorarioController extends Controller
{
    

    public function index()
    {
        $informacao = 'Todos Horários';
        $dados = Horario::orderBy('horarios.horario')->paginate(30);
//       
       // $dados = Horario::paginate(30);

            // obtém a soma do campo preço

        $numHorarios = Horario::count('id');    // obtém o número de registros

        return view('admin.horarios_list', ['linhas'=>$dados, 'informacao'=>$informacao,
                                          
                                          'numHorarios'=>$numHorarios]);
    }

    public function horariosUC(){
        $informacao = 'Dias Úteis / Sentido Centro';
        $dados = Horario::orderBy('horarios.horario')->WHERE('horarios.sentido', '=', 'C')
        ->WHERE('horarios.tipo_id', '=', '1')->paginate(30);

        $numHorarios = Horario::count('id');    // obtém o número de registros

        return view('admin.horarios_list', ['linhas'=>$dados, 'informacao'=>$informacao,
                                          
                                          'numHorarios'=>$numHorarios]);

    }
    public function horariosUB(){
       
        $informacao = 'Dias Úteis / Sentido Bairro';
        $dados = Horario::orderBy('horarios.horario')->WHERE('horarios.sentido', '=', 'B')
        ->WHERE('horarios.tipo_id', '=', '1')->paginate(30);


        $numHorarios = Horario::count('id');    // obtém o número de registros

        return view('admin.horarios_list', ['linhas'=>$dados, 'informacao'=>$informacao,
                                          
                                          'numHorarios'=>$numHorarios]);

    }

    public function horariosSB(){
       
        $informacao = 'Dias Sábados / Sentido Bairro';
        $dados = Horario::orderBy('horarios.horario')->WHERE('horarios.sentido', '=', 'B')
        ->WHERE('horarios.tipo_id', '=', '2')->paginate(30);


        $numHorarios = Horario::count('id');    // obtém o número de registros

        return view('admin.horarios_list', ['linhas'=>$dados, 'informacao'=>$informacao,
                                          
                                          'numHorarios'=>$numHorarios]);

    }

    public function horariosSC(){
       
        $informacao = 'Dias Sábados / Sentido Centro';
        $dados = Horario::orderBy('horarios.horario')->WHERE('horarios.sentido', '=', 'C')
        ->WHERE('horarios.tipo_id', '=', '2')->paginate(30);


        $numHorarios = Horario::count('id');    // obtém o número de registros

        return view('admin.horarios_list', ['linhas'=>$dados, 'informacao'=>$informacao,
                                          
                                          'numHorarios'=>$numHorarios]);

    }
    public function horariosDC(){
       
        $informacao = 'Dias Domingos / Sentido Centro';
        $dados = Horario::orderBy('horarios.horario')->WHERE('horarios.sentido', '=', 'C')
        ->WHERE('horarios.tipo_id', '=', '3')->paginate(30);


        $numHorarios = Horario::count('id');    // obtém o número de registros

        return view('admin.horarios_list', ['linhas'=>$dados, 'informacao'=>$informacao,
                                          
                                          'numHorarios'=>$numHorarios]);

    }

    public function horariosDB(){
       
        $informacao = 'Dias Domingos / Sentido Bairro';
        $dados = Horario::orderBy('horarios.horario')->WHERE('horarios.sentido', '=', 'B')
        ->WHERE('horarios.tipo_id', '=', '3')->paginate(30);


        $numHorarios = Horario::count('id');    // obtém o número de registros

        return view('admin.horarios_list', ['linhas'=>$dados, 'informacao'=>$informacao,
                                          
                                          'numHorarios'=>$numHorarios]);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $linhas = Linha::orderBy('nome')->get();
        $tipos = Tipo::orderBy('nome')->get();
        $users = User::orderBy('name')->get();
        $sentidos = Horario::sentido();
        return view('admin.horarios_form', ['tipos' => $tipos, 'linhas'=>$linhas,
        'users'=>$users, 'acao' => 1,
      'sent' => $sentidos]);
        //return view('admin.horarios_form', ['tipos' => $tipos, 'acao' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // obtém todos os campos vindos do form
        $dados = $request->all();

      

        $inc = Horario::create($dados);

        if ($inc) {
            return redirect()->route('horarios.index')
                 ->with('status', $request->nome . ' inserido com sucesso');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Horario::find($id);
        $users = User::orderBy('name')->get();
        $linhas = Linha::orderBy('nome')->get();
        $tipos = Tipo::orderBy('nome')->get();
        $sentidos = Horario::sentido();
               
        return view('admin.horarios_form', ['reg' => $reg, 'tipos' => $tipos,'linhas'=>$linhas, 
        'users'=>$users, 'sent' => $sentidos,
                                          'acao' => 2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // obtém os dados do form
        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Horario::find($id);

       

        // realiza a alteração
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('horarios.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horario = Horario::find($id);
        if ($horario->delete()) {
            return redirect()->route('horarios.index')
                            ->with('status', $horario->nome . ' Excluído!');
        }
    }
    
  
    public function relatorio() {

     
        $centroU = Horario:: join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
        ->WHERE('tipos.nome', '=', 'ÚTEIS')->WHERE('horarios.sentido', '=', 'C')
        ->select ('horarios.nome as linha',
        'horarios.descricao', 'horarios.horario','tipos.nome', 'horarios.sentido')
     ->orderBy('horarios.horario')->get();
     
    $bairroU = Horario:: join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
    ->WHERE('tipos.nome', '=', 'ÚTEIS')->WHERE('horarios.sentido', '=', 'B')
    ->select ('horarios.nome as linha',
    'horarios.descricao', 'horarios.horario','tipos.nome', 'horarios.sentido')
 ->orderBy('horarios.horario')->get();
      
 $centroS = Horario:: join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
        ->WHERE('tipos.nome', '=', 'SÁBADOS')->WHERE('horarios.sentido', '=', 'C')
        ->select ('horarios.nome as linha',
        'horarios.descricao', 'horarios.horario','tipos.nome', 'horarios.sentido')
     ->orderBy('horarios.horario')->get();
     
    $bairroS = Horario:: join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
    ->WHERE('tipos.nome', '=', 'SÁBADOS')->WHERE('horarios.sentido', '=', 'B')
    ->select ('horarios.nome as linha',
    'horarios.descricao', 'horarios.horario','tipos.nome', 'horarios.sentido')
 ->orderBy('horarios.horario')->get();

 $centroD = Horario:: join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
        ->WHERE('tipos.nome', '=', 'DOMINGOS')->WHERE('horarios.sentido', '=', 'C')
        ->select ('horarios.nome as linha',
        'horarios.descricao', 'horarios.horario','tipos.nome', 'horarios.sentido')
     ->orderBy('horarios.horario')->get();
     
    $bairroD = Horario:: join('tipos', 'horarios.tipo_id', '=', 'tipos.id')
    ->WHERE('tipos.nome', '=', 'DOMINGOS')->WHERE('horarios.sentido', '=', 'B')
    ->select ('horarios.nome as linha',
    'horarios.descricao', 'horarios.horario','tipos.nome', 'horarios.sentido')
 ->orderBy('horarios.horario')->get();

     
     
return \PDF::loadView('admin.horarios_rel',  ['centroU'=>$centroU,'bairroU'=>$bairroU,
'centroS'=>$centroS,'bairroS'=>$bairroS,
'centroD'=>$centroD,'bairroD'=>$bairroD ])->stream();
 
     }
    
    
    
    
}
