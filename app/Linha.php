<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{

    protected $fillable = ['nome', 'descricao', 'foto', 'usuers_id' ];
    public function users() {
        return $this->belongsTo('App\User');
    }

    
}
