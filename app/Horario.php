<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
   

    // identifica os campos que serão inseridos, ou seja, que estamos
    // dando permissão de serem atualizados na store
    protected $fillable = ['tipo_id', 'horario', 'sentido', 'linhas_id', 'users_id'];

    public function tipo() {
        return $this->belongsTo('App\Tipo');
    }
    public function linhas() {
        return $this->belongsTo('App\Linha');
    }
    public function users() {
        return $this->belongsTo('App\User');
    }
    public function getTipoAttribute($value) {
        return strtoupper($value);
    }

    public static function sentido() {
        return ['Bairro', 'Centro'];
    }

    public function getSentidoAttribute($value) {
        if ($value=="B") {
            return "Bairro";
        } else if ($value == "C") {
            return "Centro";
        } 
    }

    public function setSentidoAttribute($value) {
        if ($value == "Bairro") {
            $this->attributes['sentido'] = "B";
        } else if ($value == "Centro") {
            $this->attributes['sentido'] = "C";
        } 
    }        
    
}
